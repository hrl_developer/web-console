var console_window = document.getElementById('console');
var caption = document.getElementById('caption');
var input = document.getElementById('command');
var output = document.getElementById('output');

var dragWindow = {}

function valid_command(command) {

	if (command == 'cmd1') {
		return 'Команда 1';
	} else if (command == 'cmd2') {
		alert('Обработанна команда');
		return 'Success';
	} else {
		return 'Команда не найдена';
	}

}

input.onkeydown = function(event) {
	if (event.keyCode == 13) {
		
		if (this.value) {
			output.innerHTML += `
				<div class="command">
					<span class="username">pc@username</span><span class="cmd">${this.value}</span>
					<div>${valid_command(this.value)}</div>
				</div>
			`;
		}

		this.value = '';
	}
}

console_window.onclick = function() {
	input.focus();
}

caption.onmousedown = function(event) {
	if (event.which == 3) {
		return false;
	}

	dragWindow.element = console_window;
	dragWindow.downX = event.pageX - parseInt(console_window.style.left);
	dragWindow.downY = event.pageY - parseInt(console_window.style.top);

	console.log(event.pageX - parseInt(console_window.style.left));
}

document.onmousemove = function(event) {
	if (Object.keys(dragWindow)[0] == 'element') {
		dragWindow.element.style.left = `${event.clientX - dragWindow.downX}px`;
		dragWindow.element.style.top = `${event.clientY - dragWindow.downY}px`;

		console.log(event.clientX);
	}
}

document.onmouseup = function() {
	dragWindow = {}
}